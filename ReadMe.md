# Template for Language Detection project

This is project template which was ported to `Spring Boot`. Here is many advantages to use this template over original one, as `Spring Boot` :
- Is more popular , so better luck whit google, better looking your CV :).
- It has `Tomcat` included so no need to install it or configure it manually on your machine.
- No need to stick to `JSP` pages. This project template uses [Thymeleaf](https://www.thymeleaf.org/)
- [Lombok](https://projectlombok.org/) helps do more with less code (check code yourself).
- And many, many more.

As well this project template Java 13 compatible, but recommending to stick to Java 11 as it has Long Time Support. (LTS).
Note: No modules are tested!

### You will need:
- Computer, recommending with Linux.
- IntellJ IDE (Ultimate recommended as it free for students, but should work on Community edition too).
- Java 11 /Java 13 (No modules).
- Docker to test your app (optional).

### How to Run and create `war` file
- Clone template:
```bash
git clone <this repo>
```
- Open it with IntellJ
- Let if finish do indexing.
- IntelliJ should have `Maven` tab on right hand side. Open it.
- Then click on `<your-project-name>` > `Lifecycle` > `package`
- or click icon `M` and type `mvn package`
- It should start doing "something" :)
- Then finishes you should see something like:
```bash
...
[INFO] Replacing main artifact with repackaged archive
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  7.219 s
[INFO] Finished at: 2019-12-27T21:43:59Z
[INFO] ------------------------------------------------------------------------
```
- as well inside `Project` tab you should see new directory `target` containing `ngrams.war`

### Test your app with Docker
To make sure app runs on other machines too I would recommend test it using Docker. Docker is like isolated "mini-computer". So if it runs here it should run anywhere. Well as always Windows can be exception but should be fine :)

- You need have Docker installed. Linux has best support for it but it works on Mac or Windows.
- Pull and run Tomcat Docker image:
```bash
docker run -d --name myapp --rm -p 8888:8080 tomcat:jdk13-openjdk-oracle
```
Note: you can replace `-d` with `-it`. Main advantage is you can see Tomcat logs straight away.

-Copy your app to Tomcat for deployment
```bash
# I assume you are at same directory where `ngrams.war` file is
docker cp ./ngrams.war myapp:/usr/local/tomcat/webapps
```
- Tomcat should start deploy your app
```bash
# you can check logs
docker logs myapp
# or to chek last 100
#Ref: https://stackoverflow.com/a/55208523/5322506
docker logs -f --tail 100 myapp
```
- Copy `wili` text file to docker.
```bash
# first create `data` directory
docker exec myapp mkdir -p /usr/local/tomcat/data/

# then copy file 
# I asume you in project root and here is `data` directory 
# with the file `wili.text`.
# Note: change `wili.txt` to your file name.
docker cp ./data/wili.txt myapp:/usr/local/tomcat/data/

# or if you in location were is `wili` file
docker cp ./wili.txt myapp:/usr/local/tomcat/data/
```
- Go to browser and type: `localhost:8888/ngrams` and you should see same looking page as in original template.
