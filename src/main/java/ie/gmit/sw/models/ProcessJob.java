package ie.gmit.sw.models;

import lombok.Data;

@Data // Lombok magic - crates setters, getters, constructors
public class ProcessJob {
    private String cmbOptions;
    private String query;
}
