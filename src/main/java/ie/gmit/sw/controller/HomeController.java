package ie.gmit.sw.controller;

import ie.gmit.sw.models.ProcessJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;


/**
 * This is request controller  class.
 * Pages are served here.
 */
@Controller
@Slf4j // Lombok magic - adds logging
public class HomeController {

    @RequestMapping("/")
    public String viewHome() {
        return "index";
    }

    @PostMapping("/doProcess")
    public String doProcess(
             @Valid ProcessJob processJob,
                            BindingResult result,
                            Model model
    ){
        var jobNo = "<<I'm fake job :)>>";

        System.out.println("result:"+ result);
        if(result.hasErrors()){
            log.error("Error in do process: {}", result); // logging
            // Do something
        }

        log.info("<< I'm Logging>> Got: {}", processJob);

        // sending data to page
        model.addAttribute("jobNo", jobNo);
        model.addAttribute("job", processJob);

        return "hello"; // sending page `hello.html` to browser
    }
}


/*
Ref:
    - [Spring Boot + thymeleaf]: https://www.baeldung.com/spring-boot-crud-thymeleaf
 */